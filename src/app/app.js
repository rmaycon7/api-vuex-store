import bp from 'body-parser'
import cors from 'cors'
import database from './../database'
import express from 'express'
import helmet from 'helmet'
import comression from 'compression'
import index from './routes/index'
import user from './routes/user'
import auth from './routes/auth'
import userModel from '../app/models/User'
import userMiddle from './middlewares/userMiddleware'
import authenticate from './middlewares/authenticateMiddleware'
// import auth from './routes/auth'
const app = express()
database.models = {}
database.models.user = userModel
app.database = database
app.use(cors())
app.use(bp.json())
app.use(helmet())
app.use(comression())
app.disable('x-powered-by')
userMiddle(app)
user(app)
authenticate(app)
auth(app)
index(app)
export default app