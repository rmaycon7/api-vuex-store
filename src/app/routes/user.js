import UserControl from '../controllers/UserController'
import utils from '../../utils'
export default (app) => {
    const userControl = new UserControl(app.database.models.user)
    let list = ['name', 'lastname', 'email', 'id'].sort((a, b) => {
        let A = a.toLowerCase().toString(),
            B = b.toLowerCase().toString()
        if (a < b) {
            return -1
        }
        if (a > b) {
            return 1
        }
        return 0
    })
    app.route("/users")
        .get(async (req, res, next) => {
            const data = await userControl.getAll()
            data.data = data.data.map(dat => {
                return utils.formData(dat, list)
            })
            return res.status(data.statusCode).json(data.data)
        })
        .post(async (req, res, next) => {
            // console.log(req.body);

            const data = await userControl.create(req.body)
            // console.clear()
            // console.log({
            //     data: data
            // });
            // return res.end()
            return res.status(data.statusCode).json(utils.formData(data.data, list))
        })
    app.route('/users/:id')
        .get(async (req, res, next) => {
            const data = await userControl.getOne(req.params.id)
            return res.status(data.statusCode).json(utils.formData(data.data, list))

        })
        .patch(async (req, res, next) => {
            const data = await userControl.update(req.body, req.params.id)
            return res.status(data.statusCode).json(utils.formData(data.data, list))

        })
        .delete(async (req, res, next) => {
            const data = await userControl.delete(req.params.id)
            return res.status(data.statusCode).json(utils.formData(data.data, list))

        })

}