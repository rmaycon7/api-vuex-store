import errors from '../../config/errors.json'
import bcrypt from 'bcryptjs'
class User {
    constructor(User) {
        this.User = User
        // console.log(this.User);
    }
    getAll() {
        return this.User.find().then(users => {
                return {
                    data: users || errors.users_not_found,
                    statusCode: users ? 200 : errors.users_not_found.statusCode
                }
            })
            .catch(error => {
                return {
                    data: errors.users_not_found,
                    statusCode: errros.users_not_found.statusCode
                }
            })
    }
    getOne(id) {
        return this.User.findOne({
            _id: id
        }).then(user => {
            return {
                data: user || errors.user_not_found,
                statusCode: user ? 200 : errors.user_not_found.statusCode
            }
        }).catch(error => {
            return {
                data: errors.internal_error,
                statusCode: errors.internal_error.statusCode
            }
        })
    }
    create(data) {
        return this.User.create(data).then(user => {
            return {
                data: user,
                statusCode: 200
            }
        }).catch(error => {
            console.log({
                error: error
            });

            return {
                data: errors.internal_error,
                statusCode: errors.internal_error.statusCode
            }
        })
    }
    update(data, id) {
        return this.User.findOneAndUpdate({
                _id: id
            }, data, {
                new: true
            }).then(user => {
                return {
                    data: user || errors.user_not_found,
                    statusCode: user ? 200 : errors.user_not_found.statusCode
                }
            })
            .catch(error => {
                return {
                    data: errors.internal_error,
                    statusCode: errors.internal_error.statusCode
                }
            })
    }
    delete(id) {
        return this.User.findOneAndRemove({
                _id: id
            }).then(user => {
                return {
                    data: user ? {
                        code: "USER_DELETED",
                        message: "Usuário deletado com sucesso",
                        statusCode: 202,
                        name: "Deletar usuário"
                    } : errors.user_not_found,
                    statusCode: user ? 202 : errors.user_not_found.statusCode
                }
            })
            .catch(error => {
                return {
                    data: errors.internal_error,
                    statusCode: errors.internal_error.statusCode
                }
            })
    }
    compare(now, old) {
        return bcrypt.compare(now, old)
    }
}

export default User